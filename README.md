# weattr

Weather information using wttr.in

# Install:
Put `weattr` file in your `$PATH` (your `/home/user/bin` or `/home/.local/bin`)
and give the permissions to execute (`chmod 700 weattr`).
# Usage:
```
weattr
```
If you do not indicate any options it will display the current
weather.

Optional flags:
```
  -c, --color             Display coloured weather infomation.
  -f, --forecast          Forecast
  -l, --location LOCATION Specify location.
  -h, --help              Display help
  -v, --version           Display script version
```
You can indicate a specific location:
```
weattr --location paris
```
You can also display coloured weather information, if desired, by adding the `--color` flag:
```
weattr --location paris --color
```
By default, this script will show you just the current weather of your location or your selected location. If you want to show the forecast you can do so as follows:
```
weattr --location paris --color --forecast
```
For more information about how to use wttr.in services visit
help wttr.in page https://wttr.in/:help
